Windows
---

1. In the admin PowerShell init:

```shell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

2. Then install make:

```
choco install make
```

3. Install Python virtual enviroment (venv):

```
python -m venv venv
```

4. Activate it:

```
venv\Scripts\activate
```

> If you got error. Run in **admin** PowerShell, then select `Y` and try activate venv again:
> ```
> Set-ExecutionPolicy RemoteSigned
> ```


Makefile
---

init:
poetry install --no-root

format:

format-check:
