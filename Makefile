init:
	poetry install --no-root

format:
	poetry run black scripts/
	poetry run isort scripts/
	poetry run flake8 scripts/

format-check:
	poetry run black scripts/ --check
	poetry run isort scripts/ --check
	poetry run flake8 scripts/
